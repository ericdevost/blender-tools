import bpy
import csv


with open('/mnt/work/MontagesVideo/Birds-songs/001/Scene-01/Audio/aubio-toload.csv', 'r') as f:
  reader = csv.reader(f)
  coordst = list(reader)

# Convert to float
coords = []
for i in coordst:
    coords.append([float(l) for l in i])
    
# sample data
#coords = [(1,1,1), (2,2,2), (1,2,1)]

# create the Curve Datablock
curveData = bpy.data.curves.new('myCurve', type='CURVE')
curveData.dimensions = '3D'
curveData.resolution_u = 2

# map coords to spline
polyline = curveData.splines.new('NURBS')
polyline.points.add(len(coords))
for i, coord in enumerate(coords):
    x,y,z = coord
    polyline.points[i].co = (x, y, z, 1)

# create Object
curveOB = bpy.data.objects.new('myCurve', curveData)

# attach to scene and validate context
scn = bpy.context.scene
scn.objects.link(curveOB)
scn.objects.active = curveOB
curveOB.select = True
