import subprocess

class baseaudio():

    '''
    This class takes from input a video file. The process
    will:

    * Split the audio from video
    * Output an uncompressed audio file

    Example usage:

    >>> import bt_audio
    >>> bt_audio.baseaudio().run('infile', 'outfile')
    '''

    def splitaudio(self, infile, outfile):
        '''
        Split audio from video with
        ffmpeg.
        '''
        command = ['ffmpeg', '-i', infile, '-vn', '-acodec', 'pcm_s16le',
                   '-ar', '44100', '-ac', '2', outfile]

        subprocess.call(command, stdout=subprocess.PIPE,stdin=subprocess.PIPE)

    def run(self, infile, outfile):
        self.splitaudio(infile, outfile)
