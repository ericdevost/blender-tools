=====
Usage
=====

To use blender-tools in a project, simply import the module
to want to use:

.. code-block:: console

   $ python
   $ import bt_audio

See the :ref:`modules` section to read about the usage
of all modules included in ``blender-tools``.
