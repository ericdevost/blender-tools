.. blender-tools documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to blender-tools's documentation!
=========================================

|Build Status|

* `Source code on GitLab`_

.. _source code on gitlab: https://gitlab.com/ericdevost/blender-tools

This python package is intended to be used as automation tools
to be used with the 3D animation software Blender.

Getting Started
===============

.. toctree::
   :maxdepth: 2

   installation
   usage
   modules


Project Information
===================

.. toctree::
   :maxdepth: 2

   contributing
   changelog
   license



Development resources
=====================

.. toctree::
   :maxdepth: 2

   setup_script



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
