.. _modules:

=======
Modules
=======

bt_audio
========

This module is intended to be used as an audio
helper for manipulating audio files.

.. automodule:: bt_audio
   :members:
