.. highlight:: shell

============
Installation
============

Until I have setup this package to be available on pypy, you
can install it by cloning the repository and cd to it to use.

Software dependencies
=====================

* FFmpeg
* aubio
* blender
